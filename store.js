import { createStore, applyMiddleware } from 'redux'
import reducer from './reducer';
import promiseMiddleware from 'redux-promise-middleware';
import { createLogger } from 'redux-logger';
const store = createStore(reducer, applyMiddleware(promiseMiddleware(),createLogger()));
// const store = createStore(reducer, applyMiddleware(promiseMiddleware()));
export default function configureStore() {
  return store
}