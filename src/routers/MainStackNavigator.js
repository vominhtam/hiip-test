import React from 'React';
import { StackNavigator } from 'react-navigation';

import Auth from '../screens/Auth';
import Profile from '../screens/Profile';
import EditProfile from '../screens/EditProfile';



export default (StackNav = ({ initialRouteName }) => {
  const MainStackNavigator = StackNavigator(
    {
      Auth: {
        screen: Auth
      },
      Profile: {
        screen: Profile
      },
      EditProfile: {
        screen: EditProfile
      },
    },
    {
      initialRouteName: initialRouteName
    }
  );
  return <MainStackNavigator />
});