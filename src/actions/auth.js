import {
  REGISTER,
  EDIT,
  RESET
} from '../constants/actionTypes';

export const actionRegister = (data) => {
  return {
    type: REGISTER,
    payload: data
  }
};


export const actionReset = () => {
  return {
    type: RESET,
  }
};

export const actionEdit = (data) => {
  return {
    type: EDIT,
    payload: data
  }
};