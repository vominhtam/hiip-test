import {
    APP_LOAD
} from '../constants/actionTypes';

export const changeStateLoading = () => {
    return {
        type: APP_LOAD
    }
};