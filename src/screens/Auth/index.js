import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  TouchableOpacity,
  Alert
} from 'react-native';

import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Left,
  Body,
  Right,
  Title,
  Thumbnail,
  Button
} from 'native-base';

import { actionRegister } from '../../actions/auth'

import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from '../../components/ActionSheet';

import PickerComponent from '../../components/Picker';
import defaultAvatar from '../../assets/avatar/default.jpeg';

const deviceWidth = Dimensions.get('window').width;

const mapStateToProps = state => ({
  isRegisterDone: state.auth.isRegisterDone
});

const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 5;
const options = ['Cancel', 'Take a photo', 'Choose from library'];
const title = 'Select Image';

const arrayCountry = ['Viet Nam', 'Thailand'];
const arrayCity = [
  [
    'Ho Chi Minh',
    'Ha Noi',
    'Da Nang',
    'Nha Trang'
  ],
  [
    'Bangkok',
    'Nonthaburi',
    'Nakhon Ratchasima',
    'Chiang Mai'
  ]
];

const mapDispatchToProps = dispatch => ({
  sendRequest: (data) => dispatch(actionRegister(data)),
});

class Auth extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      errorMessage: '',
      country: '',
      city: '',
      arrayCity: arrayCity[0],
      isLoadingImage: false,
      imageBase64: '',
      // isLoadingImage: false,
      extension: '',
    };
    this.user = {};
    this.ActionSheet = null;
  }

  componentWillReceiveProps(nextProps){
    console.log(this.props, nextProps);
    if(!this.props.isRegisterDone && nextProps.isRegisterDone){
      this.setState({
        isAuthenticating: false
      })
    }
  }

  _getListCity = (value) => {
    let index = arrayCountry.indexOf(value);

    this.setState({
      arrayCity: arrayCity[index],
      country: value
    })
  };

  _handleSelectImage = (i) => {
    this.setState({
      isLoadingImage: true
    });
    if (i === 1) {
      ImagePicker.openCamera({
        width: deviceWidth,
        height: 400,
        // cropping: isCrop,
        includeBase64: true
      })
        .then(image => {
          this.setState({
            imageBase64: image.data,
            // isLoadingImage: false,
            extension: image.path.split('.')[1].toLowerCase(),
            isLoadingImage: false
          });
        })
        .catch(e => {
          this.setState({
            isLoadingImage: false
          });
        });
    } else if (i === 2) {
      let self = this;
      ImagePicker.openPicker({
        width: deviceWidth,
        height: 400,
        includeBase64: true
      })
        .then(image => {
          let extension;
          if (image.filename) {
            extension = image.filename.split('.')[1].toLowerCase();
          } else if (image.mime) {
            extension = image.mime.split('/')[1].toLowerCase();
          } else {
            extension = '';
          }

          self.setState({
            imageBase64: image.data,
            // isLoadingImage: false,
            extension: extension,
            isLoadingImage: false
          });
        })
        .catch(e => {
          console.log(e);
          self.setState({
            isLoadingImage: false
          });
        });
    } else {
      this.setState({
        isLoadingImage: false
      });
    }
  };

  _renderAvatar = () => {
    if (this.state.imageBase64) {
      return <Thumbnail large circle source={{uri: `data:image/png;base64,${this.state.imageBase64}`}}/>
    } else {
      return <Thumbnail large circle source={defaultAvatar}/>
    }
  };

  _handleSend = () => {
    if(!this.state.isAuthenticating)
    {
      if(
        this.user.name && this.user.phone && this.user.password
      ){
        console.log(isNaN(this.user.phone), this.user.phone.length < 9, this.user.phone.length > 13);
        if(isNaN(this.user.phone) || this.user.phone.length < 9 || this.user.phone.length > 13){
          Alert.alert(
            'Error',
            'Phone is invalid',
            { cancelable: false }
          );
          return false;
        }

        this.setState({
          isAuthenticating: true
        });

        let data = {
          name: this.user.name,
          password: this.user.password,
          phone: this.user.phone,
          city: this.state.city ? this.state.city : this.state.arrayCity[0],
          country: this.state.country ? this.state.country : arrayCountry[0],
          imageBase64: this.state.imageBase64
        };

        this.props.sendRequest(data);

      }else {
        Alert.alert(
          'Error',
          'Data invalid',
          { cancelable: false }
        )
      }
    }
  };

  render() {
    return (
      <Container>
        <Header>
          <Left/>
          <Body>
          <Title>Register</Title>
          </Body>
          <Right/>
        </Header>
        <Content style={{
          padding: 15,
        }}>
          <Form>
            <View style={{
              flex: 1,
              padding: 15,
              alignItems: 'center',
            }}>
              {
                this.state.isLoadingImage ?
                  <ActivityIndicator size="small"/> :
                  <TouchableOpacity onPress={() => this.ActionSheet.show()}>
                    {
                      this._renderAvatar()
                    }
                  </TouchableOpacity>
              }


            </View>
            <Item>
              <Input
                placeholder="Name"
                onChangeText={(text) => {
                  this.user.name = text
                }}
              />
            </Item>
            <Item>
              <Input
                secureTextEntry
                placeholder="Password"
                onChangeText={(text) => {
                  this.user.password = text
                }}
              />
            </Item>
            <Item>
              <Input
                keyboardType='numeric'
                placeholder="Phone"
                onChangeText={(text) => {
                  this.user.phone = text
                }}
              />
            </Item>
            <Item>
              <PickerComponent
                type="country"
                options={arrayCountry}
                handleSelect={(value) => this._getListCity(value)}
              />
            </Item>
            <Item>
              <PickerComponent
                parent={this.state.country}
                type='city'
                options={this.state.arrayCity}
                handleSelect={(value) => this.setState({
                  city: value
                })}
              />
            </Item>

            <Button onPress={() => this._handleSend()} full info>
              {
                this.state.isAuthenticating ? <ActivityIndicator size="small"/> :
                  <Text style={{
                    color: 'white'
                  }}>Send</Text>
              }
            </Button>

            <ActionSheet
              ref={o => (this.ActionSheet = o)}
              title={title}
              options={options}
              cancelButtonIndex={CANCEL_INDEX}
              destructiveButtonIndex={DESTRUCTIVE_INDEX}
              onPress={this._handleSelectImage}
            />
          </Form>
        </Content>
      </Container>

    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);