import { StyleSheet,Dimensions } from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#243388'
    },
    wrapper:{
        position:'absolute',
        top:0,
        left:0,
        width:deviceWidth,
        height:deviceHeight,
        paddingHorizontal: 16,
        paddingTop:30
    },
    backgroundImage: {
        flex: 1,
        width: deviceWidth,
        height: deviceHeight,
        resizeMode: 'cover',
        backgroundColor: '#243388',
    },
    slogan: {
        fontSize: 36,
        fontWeight: "bold",
        fontStyle: "normal",
        letterSpacing: -0.34,
        textAlign: "left",
        color: "#ffffff",
        backgroundColor:'transparent',
        marginBottom: 22
    },
    slogan2: {
        fontSize: 16,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: -0.11,
        textAlign: "left",
        backgroundColor:'transparent',
        color: "#ffffff"
    },
    signUpBtnText: {
        fontSize: 14,
        fontWeight: "500",
        fontStyle: "normal",
        // fontFamily: 'Roboto',
        textAlign: "right",
        color: "#ffffff",
        backgroundColor:'transparent'
    },
    signUpBtnText2: {
        fontSize: 14,
        fontWeight: "500",
        fontStyle: "normal",
        textAlign: "right",
        color: "#ffffff",
        backgroundColor:'transparent'
    },
    signInBtnText: {
        fontSize: 16,
        fontWeight: "500",
        fontStyle: "normal",
        letterSpacing: -0.3,
        textAlign: "center",
        backgroundColor:'transparent',
        color: "#ffc200"
    }
});
