import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Left,
  Body,
  Right,
  Title,
  Thumbnail,
  Button
} from 'native-base';

import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ActivityIndicator,
  AsyncStorage
} from 'react-native';

import defaultAvatar from '../../assets/avatar/default.jpeg';

const mapStateToProps = state => ({
    user: state.auth.user
});

const mapDispatchToProps = dispatch => ({
    // onGetProfile: () => dispatch(getCurrentUserInfo()),
});

class Profile extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            errorMessage: '',
        };
        this.user = {}
    }

  _renderAvatar = () => {
    if (this.props.user.imageBase64) {
      return <Thumbnail large circle source={{uri: `data:image/png;base64,${this.props.user.imageBase64}`}}/>
    } else {
      return <Thumbnail large circle source={defaultAvatar}/>
    }
  };

    render() {
        return (
          <Container>
            <Header>
              <Left/>
              <Body>
              <Title>Profile</Title>
              </Body>
              <Right/>
            </Header>
            <Content style={{
              padding: 15
            }}>
              <View style={{
                flex: 1,
                padding: 15,
                alignItems: 'center',
              }}>
                {
                  this._renderAvatar()
                }
              </View>

                <Item style={styles.item}>
                  <Text>{this.props.user.name}</Text>
                </Item>
                <Item style={styles.item}>
                  <Text>{this.props.user.password}</Text>
                </Item>
                <Item style={styles.item}>
                  <Text>{this.props.user.phone}</Text>
                </Item>
                <Item style={styles.item}>
                  <Text>{this.props.user.country}</Text>
                </Item>
                <Item last style={styles.item}>
                  <Text>{this.props.user.city}</Text>
                </Item>
                <Button  full info onPress={() => this.props.navigation.navigate('EditProfile')}>
                  <Text style={{
                    color: 'white'
                  }}>Edit</Text>
                </Button>
            </Content>
          </Container>


        )
    }
}

const styles = StyleSheet.create({
  item: {
    paddingVertical: 15
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(Profile);