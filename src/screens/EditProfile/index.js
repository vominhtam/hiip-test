import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  TouchableOpacity,
  Alert
} from 'react-native';

import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Left,
  Body,
  Right,
  Title,
  Thumbnail,
  Button
} from 'native-base';

import { actionEdit, actionReset } from '../../actions/auth'

import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from '../../components/ActionSheet';

import PickerComponent from '../../components/Picker';
import defaultAvatar from '../../assets/avatar/default.jpeg';

const deviceWidth = Dimensions.get('window').width;

const mapStateToProps = state => ({
  isEditDone: state.auth.isEditDone,
  user: state.auth.user
});

const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 5;
const options = ['Cancel', 'Take a photo', 'Choose from library'];
const title = 'Select Image';

const arrayCountry = ['Viet Nam', 'Thailand'];
const arrayCity = [
  [
    'Ho Chi Minh',
    'Ha Noi',
    'Da Nang',
    'Nha Trang'
  ],
  [
    'Bangkok',
    'Nonthaburi',
    'Nakhon Ratchasima',
    'Chiang Mai'
  ]
];

const mapDispatchToProps = dispatch => ({
  sendRequest: (data) => dispatch(actionEdit(data)),
  reset: () => dispatch(actionReset()),
});

class EditProfile extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      errorMessage: '',
      country: this.props.user.country,
      city: this.props.user.city,
      name: this.props.user.name,
      phone: this.props.user.phone,
      password: this.props.user.password,
      arrayCity: arrayCity[0],
      isLoadingImage: false,
      imageBase64: this.props.user.imageBase64,
      isEditing: false,
      // isLoadingImage: false,
      extension: '',
    };
    this.user = {};
    this.ActionSheet = null;
  }

  componentDidMount(){
    this._getListCity(this.props.user.country)
  };

  componentWillUnmount(){
    this.props.reset()
  }


  componentWillReceiveProps(nextProps){
    if(!this.props.isEditDone && nextProps.isEditDone){
      this.props.navigation.goBack();
    }
  }

  _getListCity = (value) => {
    let index = arrayCountry.indexOf(value);
    this.setState({
      arrayCity: arrayCity[index],
      country: value,
      city: ''
    })
  };

  _handleSelectImage = (i) => {
    this.setState({
      isLoadingImage: true
    });
    if (i === 1) {
      ImagePicker.openCamera({
        width: deviceWidth,
        height: 400,
        // cropping: isCrop,
        includeBase64: true
      })
        .then(image => {
          this.setState({
            imageBase64: image.data,
            // isLoadingImage: false,
            extension: image.path.split('.')[1].toLowerCase(),
            isLoadingImage: false
          });
        })
        .catch(e => {
          this.setState({
            isLoadingImage: false
          });
        });
    } else if (i === 2) {
      let self = this;
      ImagePicker.openPicker({
        width: deviceWidth,
        height: 400,
        includeBase64: true
      })
        .then(image => {
          let extension;
          if (image.filename) {
            extension = image.filename.split('.')[1].toLowerCase();
          } else if (image.mime) {
            extension = image.mime.split('/')[1].toLowerCase();
          } else {
            extension = '';
          }

          self.setState({
            imageBase64: image.data,
            // isLoadingImage: false,
            extension: extension,
            isLoadingImage: false
          });
        })
        .catch(e => {
          console.log(e);
          self.setState({
            isLoadingImage: false
          });
        });
    } else {
      this.setState({
        isLoadingImage: false
      });
    }
  };

  _renderAvatar = () => {
    if (this.state.imageBase64) {
      return <Thumbnail large circle source={{uri: `data:image/png;base64,${this.state.imageBase64}`}}/>
    } else {
      return <Thumbnail large circle source={defaultAvatar}/>
    }
  };

  _handleSend = () => {
    if(!this.state.isEditing)
    {
      if(
        this.state.name && this.state.phone && this.state.password
      ){
        if(isNaN(this.state.phone) || this.state.phone.length < 9 || this.state.phone.length > 13){
          Alert.alert(
            'Error',
            'Phone is invalid',
            { cancelable: false }
          );
          return false;
        }

        this.setState({
          isEditing: true
        });

        let data = {
          name: this.state.name,
          password: this.state.password,
          phone: this.state.phone,
          city: this.state.city ? this.state.city : this.state.arrayCity[0],
          country: this.state.country ? this.state.country : arrayCountry[0],
          imageBase64: this.state.imageBase64
        };
        this.props.sendRequest(data);


      }else {
        Alert.alert(
          'Error',
          'Data invalid',
          { cancelable: false }
        )
      }
    }
  };

  render() {
    return (
      <Container>
        <Header>
          <Left/>
          <Body>
          <Title>EDIT</Title>
          </Body>
          <Right/>
        </Header>
        <Content>
          <Form>
            <View style={{
              flex: 1,
              padding: 15,
              alignItems: 'center',
            }}>
              {
                this.state.isLoadingImage ?
                  <ActivityIndicator size="small"/> :
                  <TouchableOpacity onPress={() => this.ActionSheet.show()}>
                    {
                      this._renderAvatar()
                    }
                  </TouchableOpacity>
              }


            </View>
            <Item>
              <Input
                value={this.state.name}
                placeholder="Name"
                onChangeText={(text) => {
                  this.setState({
                    name: text
                  })
                }}
              />
            </Item>
            <Item>
              <Input
                value={this.state.password}
                secureTextEntry
                placeholder="Password"
                onChangeText={(text) => {
                  this.setState({
                    password: text
                  })
                }}
              />
            </Item>
            <Item>
              <Input
                value={this.state.phone}
                keyboardType='numeric'
                placeholder="Phone"
                onChangeText={(text) => {
                  this.setState({
                    phone: text
                  })
                }}
              />
            </Item>
            <Item>
              <PickerComponent
                type="country"
                selected={this.state.country}
                options={arrayCountry}
                handleSelect={(value) => this._getListCity(value)}
              />
            </Item>
            <Item>
              <PickerComponent
                parent={this.state.country}
                type='city'
                selected={this.state.city}
                options={this.state.arrayCity}
                handleSelect={(value) => this.setState({
                  city: value
                })}
              />
            </Item>


            <Button disabled={this.props.isLoadingImage} onPress={() => this._handleSend()} full info>
              {
                this.state.isEditing ? <ActivityIndicator size="small"/> :
                  <Text style={{
                    color: 'white'
                  }}>Send</Text>
              }
            </Button>

            <ActionSheet
              ref={o => (this.ActionSheet = o)}
              title={title}
              options={options}
              cancelButtonIndex={CANCEL_INDEX}
              destructiveButtonIndex={DESTRUCTIVE_INDEX}
              onPress={this._handleSelectImage}
            />
          </Form>
        </Content>
      </Container>

    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);