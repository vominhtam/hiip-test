import {
  REGISTER,
  EDIT,
  RESET
} from '../constants/actionTypes'

const initialState = {
  user: {},
  isRegisterDone: false,
  isEditDone: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case RESET:
    return {
      ...state,
      isEditDone: false
    };
    break;
    case REGISTER:
      return {
        ...state,
        isRegisterDone: true,
        user: action.payload
      };
      break;
      case EDIT:
      return {
        ...state,
        isEditDone: true,
        user: action.payload
      };
      break;
    default:
      return state
  }
}