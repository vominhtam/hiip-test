import {
    APP_LOAD
} from '../constants/actionTypes'
const initialState = {
    isLoading: true
};

export default (state = initialState, action) => {
    switch (action.type) {
        case APP_LOAD:
            return {
                ...state,
                isLoading: false
            };
            break;
        default:
            return state
    }
}