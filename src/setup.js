import React from 'React';
import { Provider } from 'react-redux';
import configureStore from './store';

import App from './App';

const store = configureStore();

class setup extends React.Component {
    render() {
      return (
        <Provider store={store}>
          <App/>
        </Provider>
      );
    }
  }
module.exports = setup;