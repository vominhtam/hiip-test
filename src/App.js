/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ActivityIndicator,
  AsyncStorage
} from 'react-native';

import { Root } from 'native-base';
import { changeStateLoading } from './actions/app';

import MainStackNavigator from './routers/MainStackNavigator';

const mapStateToProps = state => ({
  isLogged: state.app.isLogged,
  isLoading: state.app.isLoading,
  isRegisterDone: state.auth.isRegisterDone
});

const mapDispatchToProps = dispatch => ({
  onLoaded: () => dispatch(changeStateLoading()),
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false
    }
  }

  componentWillReceiveProps(nextProps){
    console.log('APP', this.props, nextProps);
    if(!this.props.isRegisterDone && nextProps.isRegisterDone){
      this.setState({
        isLogged: true
      })
    }
  }

  componentWillMount(){
    AsyncStorage.getItem('jwt').then((value) => {
      let token = value;
      let isLogged = false;
      if (token) {
       isLogged = true;
      }

      this.setState({
        isLogged: isLogged
      }, ()=> {
        {
          this.props.onLoaded();
        }
      })

    });
  }

  render() {
    let isLogged = this.state.isLogged;
    return (
      <Root>
        <StatusBar
          backgroundColor="#324191"
          barStyle="light-content"
        />
        {
          this.props.isLoading
            ?
            <View style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'row'
            }}>
              <ActivityIndicator size="small" color="#3f51b5" />
            </View>
            : <MainStackNavigator
              initialRouteName={!isLogged  ? 'Auth' : 'Profile'} />

        }
      </Root>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(App)