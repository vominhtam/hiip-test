import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  ActivityIndicator,
  AsyncStorage
} from 'react-native';

import {Root, Picker} from 'native-base';

const mapStateToProps = state => ({
  isAuthenticating: state.auth.isAuthenticating
});

const mapDispatchToProps = dispatch => ({
  // onGetProfile: () => dispatch(getCurrentUserInfo()),
});

class Profile extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      selected: this.props.selected ? this.props.selected : this.props.options[0],

    };
    this.user = {}
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps, this.props);
    if (
      nextProps.type === 'city' && nextProps.parent !== this.props.parent
    )
    {
      console.log(nextProps.options[0]);
      this.setState({
        selected: nextProps.options[0]
      });
    }
  }

  _onSelected = (value) => {
    this.setState({
      selected: value
    });
    this.props.handleSelect(value)
  }

  render() {
    console.log(this.state, this.props);
    return (
      <Picker
        iosHeader={this.props.header ? this.props.header : 'Select one'}
        mode={this.props.pickerType ? this.props.pickerType : 'dropdown'}
        selectedValue={this.state.selected}
        onValueChange={(value) => this._onSelected(value)}
      >
        {
          this.props.options.map((item, index) => {
            return <Picker.Item key={index} label={item} value={item}/>
          })
        }
      </Picker>

    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);