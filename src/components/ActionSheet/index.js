import { Platform } from 'react-native';
import _ActionSheetIOS from './ActionSheetIOS';
import _ActionSheetCustom from './ActionSheetCustom';

export const ActionSheetCustom = _ActionSheetCustom;

const ActionSheet =
  Platform.OS === 'ios' ? _ActionSheetIOS : _ActionSheetCustom;

export default ActionSheet;
